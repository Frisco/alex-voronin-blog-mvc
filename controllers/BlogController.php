<?php

class BlogController
{
    /**
     *
     */
    public function actionIndex()
    {
        $blogList = array();
        $blogList = Blog::getBlogList();

        require_once(ROOT . '/views/blog/index.php');
    }

    /**
     * @param $id
     */
    public function actionView($id)
    {
        if ($id) {
            $blogItem = Blog::getBlogItemById($id);
            require_once(ROOT . '/views/blog/view.php');
        }
    }

}
