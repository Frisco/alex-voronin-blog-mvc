<?php

// FRONT CONTROLLER <--

// 1. Общие настройки
ini_set('display_errors', 1);
error_reporting(E_ALL);

// инициализируем сессии
session_start();

// 2. Константа, подержащая путь к корневой директории
define('ROOT', dirname(__FILE__));

// 3. Автозагрузчик
require_once(ROOT . '/components/Autoload.php');

// 4. Подключаемся к БД
$DATA = PDO_DB::getConnection(); // создаем соединение с БД

// 5. Вызываем роутер
$router = new Router();
$router->run();

