<?php


class Blog
{

    public static function getBlogItemById($id)
    {
        $id = intval($id);

        if ($id) {
                        
            $db = PDO_DB::getConnection();
            
            $result = $db->query('SELECT * FROM blog WHERE id=' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            
            $blogItem = $result->fetch();

            return $blogItem;
        }
    }

    /**
     * Returns an array of blog items
     */
    public static function getBlogList() {

        $db = PDO_DB::getConnection();
        
        $blogList = array();
        
        $result = $db->query('SELECT *'
                . 'FROM blog '
                . 'ORDER BY date DESC, id DESC' );
                //. 'LIMIT 3');

        $i = 0;
        while($row = $result->fetch()) {
            $blogList[$i]['id'] = $row['id'];
            $blogList[$i]['title'] = $row['title'];
            $blogList[$i]['date'] = $row['date'];
            $blogList[$i]['short_content'] = $row['short_content'];
            $blogList[$i]['content'] = $row['content'];
            $i++;
        }

        return $blogList;
    }


}
