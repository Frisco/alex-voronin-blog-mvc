<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>БЛОГ - Программа Развитие Компетенций</title>
  <meta name="description" content="Блог">

  <link rel="stylesheet" href="/css/font-awesome.css">
  <!-- <link rel="stylesheet" href="/path/to/jquery.mCustomScrollbar.css" /> -->
  <link rel="stylesheet" href="/css/bootstrap.css">
  <link rel="stylesheet" href="/css/bootstrap-theme.css">
  <link rel="stylesheet" href="/css/main.css">

  <!-- IE6-8 support of HTML5 elements --> <!--[if lt IE 9]>
  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->


</head>
<!-- --------------------------------- ТЕЛО ------------------------------------ -->

<body>
<div id="body">

  <!-- ----------------- ЗАГОЛОВОК ----------------------- -->
  <header>

    <img id='header_img' src="/img/header.jpg">

    <!-- <hgroup>
        <h1>HTML5 шаблон</h1>

        <h3>стандартный</h3>
    </hgroup>
-->

    <!-- ----------------- НАВИГАЦИЯ ----------------------- -->
    <?php
    $segment = Router::getSegment();
    //print_r($segment);
    //echo $segment;
    ?>


    <nav id="nav" role="navigation" class="navbar navbar-default">
      <ul id='nav1' class="nav nav-pills"> <!-- nav-justified -->
        <li <?php if ($segment == '') echo 'class=active'; ?>>
          <a class="nav_a" href="/">Главная</a></li>
        <li <?php if ($segment == 'blog') echo 'class=active'; ?>>
          <a class="nav_a" href="/blog/">Блог</a></li>
        <li>
          <a class="nav_a" href="#">Разное</a></li>
        <li>
          <a class="nav_a" href="#">Контакты</a></li>
      </ul>



      <!-- class="<?php //if ($categoryId == $categoryItem['id']) echo 'active'; ?>" -->

      <ul id='nav2' class="nav navbar-nav nav-pills"> <!-- nav-justified -->
        <li><a id="btn_enter" href="/user/login"> <i class="fa fa-lock"> </i> Вход</a></li>
        <li><a id="btn_reg" href="/user/register"> <i class="fa fa-user"> </i> Регистрация</a></li>
      </ul>
    </nav>


  </header>


  <section>
