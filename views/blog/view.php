<?php include ROOT . '\views\layouts\header.php'; ?>

  <div id="content">

    <div class="item">
      <div class="item_header">
        <?php //echo $blogItem['id'];?>
        <h3>
          <?php echo $blogItem['title']; ?>
        </h3>
      </div>
      <div class="item_datum">
        <?php echo $blogItem['date']; ?>
      </div>

      <div class="item_content">
        <?php echo $blogItem['content']; ?>
      </div>

      <div class="item_footer">
        Подвал статьи
      </div>
    </div>
  </div>

<?php include ROOT . '/views/layouts/footer.php'; ?>