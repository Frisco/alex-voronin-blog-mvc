<!-- content begins -->
<div id="content">
  <div id="content_top">
    <div id="content_bot">

      <div id="left">
        <div class="text">

          <?php foreach ($newsList as $newsItem): ?>

            <h1>
              <a href="/news/<?php echo $newsItem['id']; ?>">
                <?php echo $newsItem['title']; ?>
              </a>
            </h1>

            <p class="meta">
              <small><?php echo $newsItem['date']; ?></small>
            </p>

            <p><?php echo $newsItem['short_content']; ?></p>

            <p class="meta">
              <a href="/news/<?php echo $newsItem['id']; ?>" class="more">Больше...</a>
              &nbsp;&nbsp;&nbsp;
              <!-- <a href="#" class="comments">Comments(33)</a> -->
            </p>
            <br/><br/>
          <?php endforeach; ?>

        </div>
        <div style="clear: both"></div>

      </div>

      <!-- content ends -->
