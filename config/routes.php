<?php

return array(

/*
  'product/([0-9]+)' => 'product/view/$1',       // actionView в ProductController

  'catalog' => 'catalog/index',         // actionIndex в CatalogController

  'category/([0-9]+)/page-([0-9]+)' => 'catalog/category/$1/$2',
  'category/([0-9]+)' => 'catalog/category/$1',   // actionCategory в CatalogController

  'cart/add/([0-9]+)' => 'cart/add/$1',   // добавить товар в корзину
  'cart/addAjax/([0-9]+)' => 'cart/addAjax/$1',   // добавить товар в корзину Аякс
  'cart' => 'cart/index',   // Корзина

  'user/register' => 'user/register',
  'user/login' => 'user/login',
  'user/logout' => 'user/logout',
  //'register' => 'user/register',

  'cabinet/edit' => 'cabinet/edit',
  'cabinet' => 'cabinet/index',

  'contacts' => 'site/contact',
*/


/* ВХОД, РЕГИСТРАЦИЯ */
  'user/register' => 'user/register',
  'user/login' => 'user/login',
  'user/logout' => 'user/logout',

/* БЛОГ */
  'blog/([0-9]+)' => 'blog/view/$1',
  'blog' => 'blog/index',

/* ГЛАВНАЯ */
  '' => 'site/index', // SiteController
);
