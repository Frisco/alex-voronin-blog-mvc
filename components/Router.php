<?php


class Router
{

  private $routes;

  public function __construct()
  {
    $routesPath = ROOT . '\config\routes.php';
    $this->routes = include($routesPath);
  }

  public function run()
  {
    // Получить строку запроса
    $uri = $this->getURI();

    // Проверить наличие такого запроса в routes.php
    foreach ($this->routes as $uriPattern => $path) {

      // Сравниваем $uriPattern и $uri
      if (preg_match("~$uriPattern~", $uri)) {

        // Получаем внутренний путь из внешнего согласно правилу.
        $internalRoute = preg_replace("~$uriPattern~", $path, $uri, 1);

        // Определить контроллер, action, параметры

        $segments = explode('/', $internalRoute);

        $controllerName = array_shift($segments) . 'Controller';
        $controllerName = ucfirst($controllerName);

        $actionName = 'action' . ucfirst(array_shift($segments));

        $parameters = $segments;

        // Подключить файл класса-контроллера
        $controllerFile = ROOT . '\controllers\\' . $controllerName . '.php';


        /*echo $controllerFile;
        echo "<br><br>";
        echo $controllerName;*/

        if (file_exists($controllerFile)) {
          include_once($controllerFile);
          $controllerObject = new $controllerName;
          //echo " $controllerFile <br> $actionName <br>";
          //print_r($parameters);

          $result = method_exists($controllerObject, $actionName);  // проверяем, есть ли метод
          if ($result != null) {
            $result == null;
            // если все ок - вызываем его с параметрами $parameters
            $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
            break;  // прерываем цикл
          } else {
            // если метода в классе нет - показываем 404
            $siteController = new SiteController();
            $result = $siteController->action404();
          }
        }
      }
    }
  }

  /**
   * Возвращает строку запроса
   */
  private function getURI()
  {
    if (!empty($_SERVER['REQUEST_URI'])) {
      return trim($_SERVER['REQUEST_URI'], '/');
    }
  }

  /**
   * Возвращает активный раздел
   */
  public static function getSegment()
  {
    // Получить строку запроса
    $uri = self::getURI();
    $segments = explode('/', $uri);
    return $segments[0];
  }

}
